color = input("Введите цвет (Пример: #0102FF): ")

def incorrect(color: str):  # Проверка на коррекность данных
    if len(color) != 7:
        return True
    if color[0] != "#":
        return True
    for char in color[1:]:
        if char not in "0123456789ABCDEF":
            return True
    return False

while incorrect(color):  # Ввод с клавитуры
    color = input("Введите цвет в 16 кодировке ")
hex = color[1:]  # убираем решетку для удобства

def is_gray(hex: str):  # Фунция 1 Является ли цвет серым
    red = hex[0:2]
    blue = hex[2:4]
    green = hex[4:6]
    if hex == "000000" or hex == "FFFFFF":  # Проверка на черный и белый
        return False
    else:
        if red == blue == green:
            return True
        else:
            return False

def to_gray (hex: str) :
    if hex == "000000":         # Проверка на черный
        tuple = [1, 1, 1]
        return tuple
    if hex == "FFFFFF":         # Проверка на белый
        tuple = [-1, -1, -1]
        return tuple
    red = int(hex[0:2],16)
    blue = int(hex[2:4],16)
    green = int(hex[4:6],16)
    average = (red + blue + green) // 3
    delta_r = average - red
    delta_b = average - blue
    delta_g = average - green
    tuple = [delta_r, delta_b, delta_g]
    return tuple

res = is_gray(hex)
res2 = to_gray(hex)
print(res)  # Вывод результата
print(res2)
